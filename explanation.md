# 重要な部分の解説
## top level code
* まずTrainerのconstructorにinfoが入ってる
* take_actionにinfo
* new_infoをprocess_experiencesに渡す
* bufferがいっぱいなら, update_model

## 全体
* actionのサイズは, brain.action_space_size

## models.py
### PPOModel
#### create_*_encoder
* modelのbottom部分の定義
* activationは与えられる

#### create_ppo_optimizer
* lossの定義
* learning rateのschedule
* optimizerを決める(Adam)

#### Continuous/DiscreateContorlModel.__init__
* activationの定義
* modelのheadの定義
* old_probsはTrainer.update_modelから与えられる

## trainer.py
### init
* statsのキーワード定義

### take_action
* feed_dict(毎stepわかるもの, observationとか)
* env.stepの実行  
ここでstepして、info=ret_dict[brain_name]してるから、なんこもbrainがあるときは使えない
* add_experiences

### add_experiences
* history_dictの更新

### process_experiences
* agentがdoneか, time_horizonを越えてないか
* その場合、advantageとかreturnを計算
* また、対応するagent_historyをreset

### update_model
* feed_dict, 特に最後にしかわからないreturnとか
* brain_historyのreset

### write_summary


## history.py

### get_gae
* GAEの計算

