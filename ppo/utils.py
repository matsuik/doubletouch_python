import glob
import os
import random

import numpy as np

opj = os.path.join


def sample_checkpoint(model_path, opponet_sampling=True):
    all_list = glob.glob(opj(model_path, "vars-*.npz"))
    all_list = sorted(all_list, key=lambda x: int(
        x.split("vars-")[1].split(".")[0]))
    if opponet_sampling:
        sampled_model_path = random.choice(all_list)
    else:
        sampled_model_path = random.choice(all_list[len(all_list) / 2:])
    return sampled_model_path


def get_latest_checkpoint(model_path):
    all_list = glob.glob(opj(model_path, "vars-*.npz"))
    all_list = sorted(all_list, key=lambda x: int(
        x.split("vars-")[1].split(".")[0]))
    return all_list[-1]


def assign_vars(sess, model, model_path, name_scope=None):
    with open(model_path, 'r') as f:
        npz = np.load(f)
        if name_scope is None:
            sess.run(model.assign_op_list,
                     feed_dict={model.var_name2placeholder_dict[var_name]: npz[var_name]
                                for var_name in model.var_name2placeholder_dict})
        else:
            sess.run(model.assign_op_list,
                     feed_dict={model.var_name2placeholder_dict[var_name]: npz["/".join(var_name.split("/")[1:])]
                                for var_name in model.var_name2placeholder_dict})
