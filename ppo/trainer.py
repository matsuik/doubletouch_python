# -*- coding: utf-8 -*-
import numpy as np
import tensorflow as tf
import copy
import os
opj = os.path.join

from ppo.history import *


class Trainer(object):
    def __init__(self, ppo_model, older_ppo_model, sess, latest_brain_name, older_brain_name, info_dict,
                 is_continuous, use_observations, use_states):
        """
        Responsible for collecting experinces and training PPO model.
        :param latest_brain_name: 
        :param older_brain_name: 
        :param ppo_model: Tensorflow graph defining model.
        :param sess: Tensorflow session.
        :param info_dict: dict of Environment BrainInfo objects.
        :param is_continuous: Whether action-space is continuous.
        :param use_observations: Whether agent takes image observations.
        :param use_states: 
        """
        self.model = ppo_model
        self.older_model = older_ppo_model

        self.sess = sess
        stats = {'cumulative_reward': [], 'episode_length': [], 'value_estimate': [],
                 'entropy': [], 'value_loss': [], 'policy_loss': [], 'learning_rate': []}
        self.stats = stats

        self.latest_brain_name = latest_brain_name
        self.older_brain_name = older_brain_name

        self.training_buffer = vectorize_history(empty_agent_history({}))

        self.brain_history_dict = empty_brain_history(
            info_dict[self.latest_brain_name])

        self.is_continuous = is_continuous
        self.use_observations = use_observations
        self.use_states = use_states

    def save_latest_model(self, model_path, steps):
        var_list = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.latest_brain_name)
        save_dict = {
            "/".join(var.name.split('/')[1:]): self.sess.run(var) for var in var_list}
        with open(opj(model_path, "vars-{}.npz".format(steps)), 'w') as f:
            np.savez_compressed(f, **save_dict)
        print('Saved Latest Model')

    def take_action(self, info_dict, env):
        """
        Decides actions given state/observation information, and takes them in environment.
        :param info_dict: dict of BrainInfo from environment.
        :param env: Environment to take actions in.
        :return: BrainInfo corresponding to new environment state.
        """

        # region latest brain
        info = info_dict[self.latest_brain_name]
        epsi = None
        feed_dict = {self.model.batch_size: len(info.states)}
        feed_dict[self.model.state_in] = info.states
        if self.model.use_lstm:
            feed_dict[self.model.recurrent_in] = info.memories

        actions, a_dist, value, ent, learn_rate = self.sess.run([self.model.output, self.model.probs,
                                                                 self.model.value, self.model.entropy,
                                                                 self.model.learning_rate],
                                                                feed_dict=feed_dict)

        action_dict = {self.latest_brain_name: actions}

        if self.model.use_lstm:
            recurrent_out = self.sess.run(
                self.model.recurrent_out, feed_dict=feed_dict)
            memory_dict = {self.latest_brain_name: recurrent_out}

        self.stats['value_estimate'].append(value)
        self.stats['entropy'].append(ent)
        self.stats['learning_rate'].append(learn_rate)
        # endregion

        # region older brain
        info = info_dict[self.older_brain_name]
        epsi = None
        feed_dict = {self.older_model.batch_size: len(info.states)}
        feed_dict[self.older_model.state_in] = info.states
        if self.model.use_lstm:
            feed_dict[self.older_model.recurrent_in] = info.memories

        actions = self.sess.run(self.older_model.output, feed_dict=feed_dict)
        action_dict[self.older_brain_name] = actions

        if self.model.use_lstm:
            recurrent_out = self.sess.run(
                self.older_model.recurrent_out, feed_dict=feed_dict)
            memory_dict[self.older_brain_name] = recurrent_out
        # endregion

        if self.model.use_lstm:
            new_info_dict = env.step(copy.deepcopy(
                action_dict), memory=memory_dict)
        else:
            new_info_dict = env.step(copy.deepcopy(action_dict))

        self.add_experiences(info_dict[self.latest_brain_name],
                             new_info_dict[self.latest_brain_name],
                             new_info_dict[self.older_brain_name],
                             epsi, action_dict[self.latest_brain_name], a_dist, value)

        return new_info_dict

    def add_experiences(self, info, next_info, older_next_info, epsi, actions, a_dist, value):
        """
        Adds experiences to each agent's experience history.
        :param info: Current BrainInfo.
        :param next_info: Next BrainInfo.
        :param epsi: Epsilon value (for continuous control)
        :param actions: Chosen actions.
        :param a_dist: Action probabilities.
        :param value: Value estimates.
        """

        for (agent, agent_history) in self.brain_history_dict.items():
            if agent in info.agents:
                idx = info.agents.index(agent)
                if not info.local_done[idx]:
                    agent_history['states'].append(info.states[idx])
                    agent_history['actions'].append(actions[idx])
                    if -0.1 < next_info.rewards[idx] < 0.1:
                        next_info.rewards[idx] *= max(
                            min(1.0 - (self.sess.run(self.model.global_step) / 5e6), 1.0),
                            0.0)
                    agent_history['rewards'].append(next_info.rewards[idx])
                    agent_history['action_probs'].append(a_dist[idx])
                    agent_history['value_estimates'].append(value[idx][0])
                    agent_history['cumulative_reward'] += next_info.rewards[idx]
                    agent_history['episode_steps'] += 1
                    agent_history['recurrent_in'].append(info.memories[idx])

    def process_experiences(self, info, time_horizon, gamma, lambd):
        """
        Checks agent histories for processing condition, and processes them as necessary.
        Processing involves calculating value and advantage targets for model updating step.
        :param info: Current BrainInfo
        :param time_horizon: Max steps for individual agent history before processing.
        :param gamma: Discount factor.
        :param lambd: GAE factor.
        """
        for i_agent in range(len(info.agents)):
            if (info.local_done[i_agent] or len(self.brain_history_dict[info.agents[i_agent]]['actions']) > time_horizon) and len(
                    self.brain_history_dict[info.agents[i_agent]]['actions']) > 0:
                if info.local_done[i_agent]:
                    value_next = 0.0
                else:
                    feed_dict = {self.model.batch_size: len(info.states)}
                    feed_dict[self.model.state_in] = info.states
                    if self.model.use_lstm:
                        feed_dict[self.model.recurrent_in] = info.memories

                    value_next = self.sess.run(
                        self.model.value, feed_dict)[i_agent]
                agent_history = vectorize_history(
                    self.brain_history_dict[info.agents[i_agent]])
                agent_history['advantages'] = get_gae(rewards=agent_history['rewards'],
                                                      value_estimates=agent_history['value_estimates'],
                                                      value_next=value_next, gamma=gamma, lambd=lambd)
                agent_history['discounted_returns'] = agent_history['advantages'] + \
                    agent_history['value_estimates']

                if len(self.training_buffer['actions']) > 0:
                    append_history(
                        global_buffer=self.training_buffer, local_buffer=agent_history)
                else:
                    set_history(global_buffer=self.training_buffer,
                                local_buffer=agent_history)

                self.brain_history_dict[info.agents[i_agent]] = empty_agent_history(
                    self.brain_history_dict[info.agents[i_agent]])
                if info.local_done[i_agent]:
                    self.stats['cumulative_reward'].append(
                        agent_history['cumulative_reward'])
                    self.stats['episode_length'].append(
                        agent_history['episode_steps'])
                    agent_history['cumulative_reward'] = 0
                    agent_history['episode_steps'] = 0

    def update_model(self, batch_size, num_epoch):
        """
        Uses training_buffer to update model. Reset experience buffers.
        :param batch_size: Size of each mini-batch update.
        :param num_epoch: How many passes through data to update model for.
        """
        total_v, total_p = 0, 0
        advantages = self.training_buffer['advantages']
        self.training_buffer['advantages'] = (
            advantages - advantages.mean()) / advantages.std()
        for i_epoch in range(num_epoch):
            training_buffer = shuffle_buffer(self.training_buffer)
            for i_batch in range(len(training_buffer['actions']) // batch_size):
                start = i_batch * batch_size
                end = (i_batch + 1) * batch_size
                feed_dict = {self.model.returns_holder: training_buffer['discounted_returns'][start:end],
                             self.model.advantage: np.vstack(training_buffer['advantages'][start:end]),
                             self.model.old_probs: np.stack(training_buffer['action_probs'][start:end])}

                feed_dict[self.model.action_holder] = np.stack(
                    training_buffer['actions'][start:end])

                feed_dict[self.model.state_in] = np.vstack(
                    training_buffer['states'][start:end])
                if self.model.use_lstm:
                    feed_dict[self.model.recurrent_in] = np.vstack(
                        training_buffer['recurrent_in'][start:end])

                v_loss, p_loss, _ = self.sess.run([self.model.value_loss, self.model.policy_loss,
                                                   self.model.update_batch], feed_dict=feed_dict)
                total_v += v_loss
                total_p += p_loss
        self.stats['value_loss'].append(total_v)
        self.stats['policy_loss'].append(total_p)
        self.training_buffer = vectorize_history(empty_agent_history({}))
        for key in self.brain_history_dict:
            self.brain_history_dict[key] = empty_agent_history(
                self.brain_history_dict[key])

    def write_summary(self, summary_writer, steps):
        """
        Saves training statistics to Tensorboard.
        :param summary_writer: writer associated with Tensorflow session.
        :param steps: Number of environment steps in training process.
        """
        print("Mean Reward: {0}".format(
            np.mean(self.stats['cumulative_reward'])))
        summary = tf.Summary()
        is_there_nan = False
        for key in self.stats:
            if len(self.stats[key]) > 0:
                stat = float(np.mean(self.stats[key]))
                summary.value.add(
                    tag='Info/{}_mean'.format(key), simple_value=stat)
                is_there_nan += np.isnan(stat)

                stat = float(np.std(self.stats[key]))
                summary.value.add(
                    tag='Info/{}_std'.format(key), simple_value=stat)
                is_there_nan += np.isnan(stat)

                stat = float(np.median(self.stats[key]))
                summary.value.add(
                    tag='Info/{}_median'.format(key), simple_value=stat)
                is_there_nan += np.isnan(stat)

                stat = float(np.max(self.stats[key]))
                summary.value.add(
                    tag='Info/{}_max'.format(key), simple_value=stat)
                is_there_nan += np.isnan(stat)

                stat = float(np.min(self.stats[key]))
                summary.value.add(
                    tag='Info/{}_min'.format(key), simple_value=stat)
                is_there_nan += np.isnan(stat)

                self.stats[key] = []
        summary_writer.add_summary(summary, steps)
        summary_writer.flush()
        return is_there_nan
