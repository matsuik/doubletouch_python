# -*- coding: utf-8 -*-
import numpy as np
import tensorflow as tf
import tensorflow.contrib.layers as c_layers
from tensorflow.python.tools import freeze_graph
from tensorflow.contrib import rnn
from unityagents import UnityEnvironmentException


def create_agent_model(env, lr=1e-4, h_size=128, lstm_size=64, epsilon=0.2, beta=1e-3, max_step=5e6,
                       use_lstm=False, train_model=True, decay_learning_rate=True):
    """
    Takes a Unity environment and model-specific hyper-parameters and returns the
    appropriate PPO agent model for the environment.
    :param env: a Unity environment.
    :param lr: Learning rate.
    :param h_size: Size of hidden layers/
    :param epsilon: Value for policy-divergence threshold.
    :param beta: Strength of entropy regularization.
    :return: a sub-class of PPOAgent tailored to the environment.
    :param max_step: Total number of training steps.
    """
    brain_name = env.brain_names[0]
    brain = env.brains[brain_name]
    return DiscreteControlModel(lr, brain, h_size, lstm_size, epsilon, beta, max_step, use_lstm, train_model,
                                decay_learning_rate)


def save_model(sess, saver, model_path="./", steps=0):
    """
    Saves current model to checkpoint folder.
    :param sess: Current Tensorflow session.
    :param model_path: Designated model path.
    :param steps: Current number of steps in training process.
    :param saver: Tensorflow saver for session.
    """
    last_checkpoint = model_path + '/model-' + str(steps) + '.cptk'
    saver.save(sess, last_checkpoint)
    tf.train.write_graph(sess.graph_def, model_path,
                         'raw_graph_def.pb', as_text=False)
    print("Saved Model")


def export_graph(model_path, env_name="env", target_nodes="action", use_lstm=False):
    """
    Exports latest saved model to .bytes format for Unity embedding.
    :param model_path: path of model checkpoints.
    :param env_name: Name of associated Learning Environment.
    :param target_nodes: Comma separated string of needed output nodes for embedded graph.
    """
    if use_lstm:
        target_nodes = ','.join([target_nodes, "recurrent_out"])
    ckpt = tf.train.get_checkpoint_state(model_path)
    freeze_graph.freeze_graph(input_graph=model_path + '/raw_graph_def.pb',
                              input_binary=True,
                              input_checkpoint=ckpt.model_checkpoint_path,
                              output_node_names=target_nodes,
                              output_graph=model_path + '/' + env_name + '.bytes',
                              clear_devices=True, initializer_nodes="", input_saver="",
                              restore_op_name="save/restore_all", filename_tensor_name="save/Const:0")


class PPOModel(object):
    def create_continuous_state_encoder(self, s_size, h_size, lstm_size, num_streams, activation):
        """
        Builds a set of hidden state encoders.
        :param s_size: state input size.
        :param h_size: Hidden layer size.
        :param num_streams: Number of state streams to construct.
        :param activation: What type of activation function to use for layers.
        :return: List of hidden layer tensors.
        """
        self.state_in = tf.placeholder(
            shape=[None, s_size], dtype=tf.float32, name='state')
        streams = []
        for i in range(num_streams):
            hidden_1 = tf.layers.dense(
                self.state_in, h_size, activation=activation, name='hidden_1')
            hidden_2 = tf.layers.dense(
                hidden_1, h_size, activation=activation, name='hidden_2')
            hidden_lstm = tf.layers.dense(
                hidden_1, lstm_size, activation=activation, name='hidden_lstm')

            if self.use_lstm:
                self.recurrent_in = tf.placeholder(
                    tf.float32, shape=[None, 2 * lstm_size], name='recurrent_in')
                c_state, h_state = tf.split(
                    self.recurrent_in, num_or_size_splits=2, axis=1)
                lstm_state = rnn.LSTMStateTuple(c_state, h_state)
                lstm_cell = rnn.BasicLSTMCell(lstm_size)
                lstm_output, new_lstm_state = lstm_cell(
                    hidden_lstm, lstm_state)
                tmp = tf.concat(new_lstm_state, axis=1)
                self.recurrent_out = tf.identity(tmp, name="recurrent_out")
                hidden_2 = tf.concat([hidden_2, lstm_output], axis=1)

            streams.append(hidden_2)
        return streams

    def create_ppo_optimizer(self, probs_list, old_probs_list, value, entropy_list, beta, epsilon, lr, max_step):
        """
        Creates training-specific Tensorflow ops for PPO models.
        :param probs: Current policy probabilities
        :param old_probs: Past policy probabilities
        :param value: Current value estimate
        :param beta: Entropy regularization strength
        :param entropy: Current policy entropy
        :param epsilon: Value for policy-divergence threshold
        :param lr: Learning rate
        :param max_step: Total number of training steps.
        """
        self.returns_holder = tf.placeholder(
            shape=[None], dtype=tf.float32, name='discounted_rewards')
        self.advantage = tf.placeholder(
            shape=[None, 1], dtype=tf.float32, name='advantages')

        r_theta = sum(
            [probs / old_probs for (probs, old_probs) in zip(probs_list, old_probs_list)]) / self.n_char
        p_opt_a = r_theta * self.advantage
        p_opt_b = tf.clip_by_value(
            r_theta, 1 - epsilon, 1 + epsilon) * self.advantage
        self.policy_loss = -tf.reduce_mean(tf.minimum(p_opt_a, p_opt_b))

        self.value_loss = tf.reduce_mean(tf.squared_difference(self.returns_holder,
                                                               tf.reduce_sum(value, axis=1)))

        self.loss = self.policy_loss + self.value_loss - \
            beta * tf.reduce_mean(entropy_list)

        self.global_step = tf.Variable(
            0, trainable=False, name='global_step', dtype=tf.int64)

        if self.decay_learing_rate:
            self.learning_rate = tf.train.polynomial_decay(lr, self.global_step,
                                                           max_step, 1e-10,
                                                           power=1.0)
        else:
            self.learning_rate = tf.train.polynomial_decay(lr, self.global_step,
                                                           max_step, end_learning_rate=lr,
                                                           power=1.0)
        optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        self.update_batch = optimizer.minimize(self.loss)

        self.increment_step = tf.assign(self.global_step, self.global_step + 1)


class DiscreteControlModel(PPOModel):
    def __init__(self, lr, brain, h_size, lstm_size, epsilon, beta, max_step, use_lstm, train_model, decay_learing_rate):
        """
        Creates Discrete Control Actor-Critic model.
        :param brain: State-space size (brain paramsを使ってるだけやから、latestだけでいい
        :param h_size: Hidden layer size
        """
        self.use_lstm = use_lstm
        self.train_model = train_model
        self.decay_learing_rate = decay_learing_rate
        s_size = brain.state_space_size

        hidden = self.create_continuous_state_encoder(
            s_size, h_size, lstm_size, 1, tf.nn.relu)[0]

        a_size = 18
        self.n_char = brain.action_space_size

        self.batch_size = tf.placeholder(
            shape=None, dtype=tf.int64, name='batch_size')

        probs_list = []
        action_list = []
        entropy_list = []
        self.action_holder = tf.placeholder(
            shape=[None, self.n_char], dtype=tf.int64)
        self.old_probs = tf.placeholder(
            shape=[None, self.n_char, a_size], dtype=tf.float32, name='old_probabilities')
        responsible_probs_list = []
        old_responsible_probs_list = []

        for i_char in range(self.n_char):
            w_shape = [hidden.get_shape()[1].value, a_size]
            w = tf.get_variable('W_{}'.format(i_char),
                                shape=w_shape, dtype=tf.float32)
            w_weight_noise = tf.get_variable(
                'w_weight_noise_{}'.format(i_char), shape=w_shape, initializer=c_layers.xavier_initializer(), dtype=tf.float32)
            w_with_noise = w + w_weight_noise * \
                tf.truncated_normal(
                    shape=w_shape, stddev=0.1, dtype=tf.float32)

            bias = tf.get_variable('bias_{}'.format(
                i_char), shape=[a_size], dtype=tf.float32)
            w_bias_noise = tf.get_variable(
                'w_bias_noise_{}'.format(i_char), shape=[a_size], dtype=tf.float32)
            bias_with_noise = bias + w_bias_noise * \
                tf.truncated_normal(
                    shape=[a_size], stddev=0.1, dtype=tf.float32)

            policy = tf.matmul(hidden, w_with_noise) + bias_with_noise

            probs = tf.nn.softmax(policy)
            probs_list.append(probs)

            if train_model:
                action = tf.multinomial(policy, 1)
            else:  # cannot use multinomial in android
                sigma = tf.placeholder(
                    dtype=tf.float32, name='action_noise_stddev_{}'.format(i_char))
                action = tf.cast(tf.reshape(tf.argmax(
                    policy + tf.truncated_normal(shape=tf.shape(policy), stddev=sigma), axis=1), shape=(-1, 1)),
                    tf.int32)

            action_list.append(action)

            entropy = - \
                tf.reduce_sum(
                    probs * tf.log(tf.clip_by_value(probs, 1e-8, 1.0)), axis=1)
            entropy_list.append(entropy)

            selected_actions = c_layers.one_hot_encoding(
                self.action_holder[:, i_char], a_size)

            responsible_probs = tf.reduce_sum(
                probs * selected_actions, axis=1)
            responsible_probs_list.append(responsible_probs)

            old_responsible_probs = tf.reduce_sum(
                self.old_probs[:, i_char] * selected_actions, axis=1)
            old_responsible_probs_list.append(old_responsible_probs)

        self.value = tf.layers.dense(
            hidden, 1, activation=None)

        self.entropy = sum(entropy_list) / self.n_char

        self.create_ppo_optimizer(responsible_probs_list, old_responsible_probs_list,
                                  self.value, self.entropy, beta, epsilon, lr, max_step)

        print(action)
        self.output = tf.identity(
            tf.cast(tf.stack([action[:, 0] for action in action_list], axis=1), tf.int64), name='action')

        self.probs = tf.concat(
            [tf.reshape(probs, (-1, 1, tf.shape(probs)[1])) for probs in probs_list], axis=1)

        var_list = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,
                                     scope=tf.get_default_graph().get_name_scope())
        if tf.get_default_graph().get_name_scope() == '':
            self.var_name2placeholder_dict = {
                var.name: tf.placeholder(var.dtype) for var in var_list}
        else:
            self.var_name2placeholder_dict = {
                var.name: tf.placeholder(var.dtype)
                for var in var_list}

        self.assign_op_list = [
            tf.assign(var, self.var_name2placeholder_dict[var.name]) for var in var_list]
