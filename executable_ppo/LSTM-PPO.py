
# coding: utf-8

# # Unity ML Agents
# ## Proximal Policy Optimization (PPO)
# Contains an implementation of PPO as described [here](https://arxiv.org/abs/1707.06347).

# In[1]:


import numpy as np
import os
import tensorflow as tf
import time
import random
import json

import glob
import shutil

import argparse

from ppo.history import *
from ppo.models import *
from ppo.utils import *
from ppo.trainer import Trainer
from unityagents import *

opj = os.path.join

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('run_path', type=str)
parser.add_argument('i_seed', type=int)

parser.add_argument('env_path_woext', type=str)
parser.add_argument('-gamma', type=float, default=0.99)
parser.add_argument('-opponent-sampling', type=int, default=1)
parser.add_argument('-decay-learning-rate', type=int, default=1)
parser.add_argument('--debug', action='store_true', default=False)
parser.add_argument('--mac', action='store_true', default=False)
parser.add_argument('-beta', type=float, default=1e-2)
args = parser.parse_args()

env_base_name = os.path.basename(
    os.path.normpath(args.env_path_woext)).split('.')[0]

# The sub-directory name for model and summary statistics
run_path = args.run_path
run_path += env_base_name
if not args.gamma == 0.99:
    run_path += '_gamma_{}_'.format(args.gamma)
if not args.opponent_sampling:
    run_path += '_half_sampling_'
if not args.decay_learning_rate:
    run_path += '_stop_decaying_'
run_path += str(args.i_seed)

gamma = args.gamma  # Reward discount rate.
# Name of the training environment file.


env_name = args.env_path_woext
opponet_sampling = args.opponent_sampling
decay_learning_rate = args.decay_learning_rate


max_steps = 4e8  # Set maximum number of steps to run environment.
load_model = False  # Whether to load a saved model.
train_model = True  # Whether to train the model.

buffer_size = 20480
# Frequency at which to save training statistics.
summary_freq = buffer_size * 10
save_freq = summary_freq  # Frequency at which to save model.

# Algorithm-specific parameters for tuning
lambd = 0.95  # Lambda parameter for GAE.
# How many steps to collect per agent before adding to buffer.
time_horizon = 2048
beta = args.beta  # Strength of entropy regularization
num_epoch = 3  # Number of gradient descent steps per batch of experiences.
# Acceptable threshold around ratio of old and new policy probabilities.
epsilon = 0.2
# How large the experience buffer should be before gradient descent.
learning_rate = 3e-4  # Model learning rate.
hidden_units = 256  # Number of units in hidden layer.
lstm_size = 64
batch_size = 32  # How many experiences per gradient descent update step.
use_lstm = True
least_sample_interval = 100

if args.mac:
    time_horizon = 2048
    buffer_size = 2048


if args.debug:
    max_steps = 2000
    buffer_size = 500
    summary_freq = buffer_size * 2
    save_freq = summary_freq
    time_horizon = 100


env = UnityEnvironment(file_name=env_name, worker_id=random.randint(0, 60000))
print(str(env))
print(env.brain_names)

# CRUCIAL
latest_brain_name = "LatestBrain"
older_brain_name = "ElderBrain"

# In[2]:


tf.reset_default_graph()

# Create the Tensorflow model graph
with tf.variable_scope(latest_brain_name, initializer=tf.truncated_normal_initializer(stddev=1e-5)):
    ppo_model = create_agent_model(env, lr=learning_rate,
                                   h_size=hidden_units,  lstm_size=lstm_size,
                                   epsilon=epsilon,
                                   beta=beta, max_step=max_steps,
                                   use_lstm=use_lstm, train_model=train_model,
                                   decay_learning_rate=decay_learning_rate)

with tf.variable_scope(older_brain_name, initializer=tf.truncated_normal_initializer(stddev=1e-5)):
    older_ppo_model = create_agent_model(env, lr=learning_rate,
                                         h_size=hidden_units,  lstm_size=lstm_size,
                                         epsilon=epsilon,
                                         beta=beta, max_step=max_steps,
                                         use_lstm=use_lstm, train_model=train_model,
                                         decay_learning_rate=decay_learning_rate)

is_continuous = (
    env.brains[latest_brain_name].action_space_type == "continuous")
use_observations = (env.brains[latest_brain_name].number_observations > 0)
use_states = (env.brains[latest_brain_name].state_space_size > 0)

model_path = 'models/{}'.format(run_path)

summary_path = 'summaries/{}'.format(run_path)

# assert not os.path.exists(model_path)

if not os.path.exists(model_path):
    os.makedirs(model_path)

# assert not os.path.exists(summary_path)

if not os.path.exists(summary_path):
    os.makedirs(summary_path)

basic_entry = dict([t for t in globals().items()
                    if type(t[1]) == int or type(t[1]) == float])

with open(opj(summary_path, 'params.json'), mode='w') as f:
    json.dump(basic_entry, f, indent=2)

# In[3]:


init = tf.global_variables_initializer()
try:
    with tf.Session() as sess:
        # Instantiate model parameters
        sess.run(init)
        steps = sess.run(ppo_model.global_step)
        last_sample_step = steps
        summary_writer = tf.summary.FileWriter(summary_path)
        info_dict = env.reset(train_mode=train_model)
        trainer = Trainer(ppo_model, older_ppo_model, sess, latest_brain_name, older_brain_name,
                          info_dict, is_continuous, use_observations, use_states)
        start_time = time.time()
        while steps <= max_steps:
            # Decide and take an action
            new_info_dict = trainer.take_action(info_dict, env)
            info_dict = new_info_dict
            trainer.process_experiences(
                info_dict[latest_brain_name], time_horizon, gamma, lambd)

            if info_dict[older_brain_name].local_done[0] and steps > save_freq and steps > last_sample_step + least_sample_interval:
                tmp = time.time()
                assign_vars(sess, older_ppo_model, sample_checkpoint(model_path),
                            name_scope=older_brain_name)
                last_sample_step = steps

            if len(trainer.training_buffer['actions']) > buffer_size and train_model:
                # Perform gradient descent with experience buffer
                trainer.update_model(batch_size, num_epoch)

            # LOGGING
            if steps % summary_freq == 0 and train_model:
                # Write training statistics to tensorboard.
                is_there_nan = trainer.write_summary(summary_writer, steps)
                print(run_path, steps, time.time() - start_time)

                if is_there_nan and steps != 0:
                    assign_vars(sess, ppo_model, get_latest_checkpoint(model_path),
                                name_scope=latest_brain_name)
                elif steps != 0 and train_model:
                    trainer.save_latest_model(model_path, steps)

            # STEP FORWARD
            steps += 1
            sess.run(ppo_model.increment_step)

        # Final save Tensorflow model
        if steps != 0 and train_model:
            trainer.save_latest_model(model_path, steps)

except Exception as e:
    print(e)

finally:
    tf.reset_default_graph()
    ppo_model = create_agent_model(env, lr=learning_rate,
                                   h_size=hidden_units,  lstm_size=lstm_size,
                                   epsilon=epsilon,
                                   beta=beta, max_step=max_steps,
                                   use_lstm=use_lstm, train_model=True,
                                   decay_learning_rate=decay_learning_rate)

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()
    with tf.Session() as sess:
        # Instantiate model parameters
        sess.run(init)
        assign_vars(sess, ppo_model, get_latest_checkpoint(model_path),
                    name_scope=None)

        save_model(sess, model_path=model_path, steps=steps, saver=saver)

    export_graph(model_path, os.path.basename(
        run_path) + 'multinomial', use_lstm=use_lstm)

    tf.reset_default_graph()
    ppo_model = create_agent_model(env, lr=learning_rate,
                                   h_size=hidden_units,  lstm_size=lstm_size,
                                   epsilon=epsilon,
                                   beta=beta, max_step=max_steps,
                                   use_lstm=use_lstm, train_model=False,
                                   decay_learning_rate=decay_learning_rate)

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()
    with tf.Session() as sess:
        # Instantiate model parameters
        sess.run(init)
        assign_vars(sess, ppo_model, get_latest_checkpoint(model_path),
                    name_scope=None)

        save_model(sess, model_path=model_path, steps=steps, saver=saver)

    export_graph(model_path, os.path.basename(run_path), use_lstm=use_lstm)
