import os
import argparse
import subprocess
parser = argparse.ArgumentParser()
parser.add_argument('run_path', type=str)
parser.add_argument('num_seed', type=int)
parser.add_argument('env_path_woext', type=str)
parser.add_argument('-params', type=str, default='')

args = parser.parse_args()

for i_seed in range(args.num_seed):
    cmd = 'nohup python executable_ppo/LSTM-PPO.py {} {} {} {} &'.format(
        args.run_path,
        i_seed,
        args.env_path_woext,
        args.params
    )
    subprocess.Popen(cmd, shell=True)
