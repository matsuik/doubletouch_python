from ppo import utils
import argparse
import glob
import os
opj = os.path.join
parser = argparse.ArgumentParser()
parser.add_argument('model_path_wo_seed', type=str)
args = parser.parse_args()

dirs = glob.glob(args.model_path_wo_seed + '*')
print('deleting not-latest vars in {}'.format(dirs))

for model_path in dirs:
    all_list = glob.glob(opj(model_path, "vars-*.npz"))
    all_list = sorted(all_list, key=lambda x: int(
        x.split("vars-")[1].split(".")[0]))
    for var in all_list[:-1]:
        os.remove(var)
